# Layers

A script to export and import a unity project's custom layers for a given VRChat prefab.
 Adds a window option called "VRC Prefabs" with two options under it: "Setup Layers" and "Export Layers". On first startup or import with the new assets, this script will automatically show the Setup Layers window to prompt the user this prefab uses custom layers.
